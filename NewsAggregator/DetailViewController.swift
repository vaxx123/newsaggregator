//
//  DetailViewController.swift
//  NewsAggregator
//
//  Created by ss on 10/04/2017.
//  Copyright © 2017 test. All rights reserved.
//

import UIKit
import SafariServices

class DetailViewController: UIViewController, SFSafariViewControllerDelegate {

    @IBOutlet private var stackView: UIStackView!
    private var safariVC: SFSafariViewController!
    var url: String?

    private func configureView() {
        guard let url = url, let resourceURL = URL(string: url) else {
            return
        }
        safariVC = SFSafariViewController(url: resourceURL)
        safariVC.delegate = self
        self.addChildViewController(safariVC)
        stackView.addArrangedSubview(safariVC.view)
        safariVC.didMove(toParentViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        let nav = navigationController?.navigationController
        nav?.popToRootViewController(animated: true)
    }

}

