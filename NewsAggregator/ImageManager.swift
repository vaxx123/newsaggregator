//
//  ImageManager.swift
//  NewsAggregator
//
//  Created by User on 4/10/17.
//  Copyright © 2017 test. All rights reserved.
//

import Foundation
import UIKit

class ImageManager {
    
    static let sharedInstance = ImageManager()
    
    private var imagesStorage = [String: UIImage]()
    
    private init(){}
    
    func getImage(strUrl: String, completion: @escaping (UIImage) -> Void) {
        
        if let image = imagesStorage[strUrl] {
            completion(image)
            return
        }
        
        DispatchQueue.global(qos: .default).async {
            if let url = URL(string: strUrl), let data = try? Data(contentsOf: url) {
                DispatchQueue.main.async {
                    let image = UIImage(data: data)
                    self.imagesStorage[strUrl] = image
                    completion(image!)
                }
            }
        }
        
    }
    
    
}
