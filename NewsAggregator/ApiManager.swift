//
//  ApiManager.swift
//  NewsAggregator
//
//  Created by ss on 10/04/2017.
//  Copyright © 2017 test. All rights reserved.
//

import Foundation

class ApiManager {
    
    static let sharedInstance = ApiManager()
    
    private init(){}
    
    enum DataErrors: Error {
        case parseError
    }
    
    private let apiKey = "96e3455997804140b67f67c4028e6393"
    private let articleEndpoint = "https://newsapi.org/v1/articles"
    private let sourcesEndpoint = "https://newsapi.org/v1/sources"
    private let kParamNameKey = "paramName"
    private let kParamValueKey = "paramValue"
    private let kParamApiKey = "apiKey"
    private let kParamSourceKey = "source"
    private let kParamLanguageKey = "language"
    
    private let kParseKeyArticles = "articles"
    private let kParseKeyTitle = "title"
    private let kParseKeyDescription = "description"
    private let kParseKeyUrl = "url"
    private let kParseKeyImageUrl = "urlToImage"
    private let kParseKeySources = "sources"
    private let kParseKeySourceId = "id"
    private let kParamDefaultLanguage = "en"

    private var newsSources: [String]?
    
    private var loadingFlag: Bool = false
    
    func loadData(completion: @escaping ([Article]?, Error?) -> Void) {
        if loadingFlag {
            return
        }
        
        loadingFlag = true
        
        if let sources = newsSources, sources.count > 0 {
            self.getArticles(withCompletion: completion)
            return;
        }
        
        getSources {
            error in
            if error != nil {
                self.loadingFlag = false;
                completion(nil, error)
                return
            }
            
            self.getArticles(withCompletion: completion)
        }
    }
    
    private func getArticles(withCompletion completion: @escaping ([Article]?, Error?) -> Void) {
        
        guard let sources = newsSources, sources.count > 0 else {
            return
        }
        
        let randomIndex = Int(arc4random_uniform(UInt32(sources.count)))
        let randomSource = sources[randomIndex]
        
        let params = [(kParamApiKey, apiKey),(kParamSourceKey, randomSource)]
        
        guard let url = buildUrl(host: articleEndpoint, params: params) else {
            return
        }

        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            
            guard error == nil, let data = data else {
                completion(nil, error)
                return;
            }
            
            guard let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) else {
                completion(nil, DataErrors.parseError)
                return
            }
            
            let articles = self.parseArticles(data: json as! [String : Any])
            
            completion(articles, nil)
            self.loadingFlag = false
        }
        
        task.resume()
    }
    
    private func getSources(withCompletion completion: @escaping (Error?) -> Void) {
        let params = [(kParamApiKey, apiKey), (kParamLanguageKey, kParamDefaultLanguage)]
        
        guard let url = buildUrl(host: sourcesEndpoint, params: params) else {
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            
            guard error == nil, let data = data else {
                completion(error)
                return;
            }
            
            guard let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) else {
                completion(nil)
                return
            }
            
            let sources = self.parseSources(data: json as! [String : Any])
            self.newsSources = sources
            completion(nil)
        }
        
        task.resume()
    }
    
    private func buildUrl(host: String, params: [(String, String)]) -> URL? {
        var queryParams = [URLQueryItem]()
        
        for param in params {
            let component = URLQueryItem(name: param.0, value: param.1)
            queryParams.append(component)
        }
        
        guard var urlComponents = URLComponents(string: host) else {
            return nil
        }
        urlComponents.queryItems = queryParams
        
        return urlComponents.url
    }
    
    private func parseArticles(data: [String : Any]) -> [Article]? {
        guard let articlesArray = data[kParseKeyArticles] as? [[String: Any]] else {
            return nil
        }
        
        var articles = [Article]()

        for articleData in articlesArray {
            
            guard let title = articleData[kParseKeyTitle] as? String,
                let description = articleData[kParseKeyDescription] as? String,
                let url = articleData[kParseKeyUrl] as? String,
                let imageUrl = articleData[kParseKeyImageUrl] as? String else {
                return nil
            }
            
            let article = Article(title: title, description: description, url: url, imageUrl: imageUrl)
            articles.append(article)
        }
        
        return articles
    }
    
    private func parseSources(data: [String :Any]) -> [String]? {
        guard let sourcesArray = data[kParseKeySources] as? [[String: Any]] else {
            return nil
        }
        
        var sources = [String]()
        
        for sourceData in sourcesArray {
            
            guard let id = sourceData[kParseKeySourceId] as? String else {
                    return nil
            }
        
            sources.append(id)
        }
        
        return sources
    }
}
