//
//  Article.swift
//  NewsAggregator
//
//  Created by ss on 10/04/2017.
//  Copyright © 2017 test. All rights reserved.
//

import Foundation

class Article {
    let title: String
    let description: String
    let url: String
    let imageUrl: String
    
    init(title: String, description: String, url: String, imageUrl: String) {
        self.title = title
        self.description = description
        self.url = url
        self.imageUrl = imageUrl
    }
}
