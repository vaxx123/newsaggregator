//
//  MasterViewController.swift
//  NewsAggregator
//
//  Created by ss on 10/04/2017.
//  Copyright © 2017 test. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    
    private var objects = [Article]()
    private var cache = NSCache<Article, UIImage>()
    
    private var apiManager = ApiManager.sharedInstance

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        
        let nib = UINib(nibName: "NewsTableViewCell", bundle: Bundle.main)
        tableView.register(nib, forCellReuseIdentifier: "NewsCell")
        tableView.rowHeight = 120;
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(pulledToRefresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
        
        loadArticles()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    private func loadArticles() {
        apiManager.loadData { articles, error in
            DispatchQueue.main.async {
                if let error = error {
                    self.presentAlert(withMessage: error.localizedDescription)
                    return
                }
                guard let articles = articles else {
                    return
                }
                self.objects += articles
                self.tableView.reloadData()
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let object = objects[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.url = object.url
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath)
        let article = objects[indexPath.row]
        
        if let cell = cell as? NewsTableViewCell {
            cell.configureCell(withArticle: article)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showDetail", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == objects.count - 2) {
            loadArticles()
        }
    }
    
    private func presentAlert(withMessage message: String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default) {action in return}
        alertController.addAction(action)
        present(alertController, animated: true)
        tableView.refreshControl?.endRefreshing()
    }
    
    @objc private func pulledToRefresh() {
        apiManager.loadData { articles, error in
            if let error = error {
                self.presentAlert(withMessage: error.localizedDescription)
                self.tableView.refreshControl?.endRefreshing()
                return
            }
            
            guard let data = articles, data.count > 0 else {
                self.tableView.refreshControl?.endRefreshing()
                return
            }
            
            self.objects = data
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.tableView.refreshControl?.endRefreshing()
            }
        }
    }

}

