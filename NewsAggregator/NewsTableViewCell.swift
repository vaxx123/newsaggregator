//
//  NewsTableViewCell.swift
//  NewsAggregator
//
//  Created by ss on 10/04/2017.
//  Copyright © 2017 test. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {

    @IBOutlet private var newsTitle: UILabel!;
    @IBOutlet private var newsDescription: UILabel!;
    @IBOutlet private var newsImage: UIImageView!;

    func configureCell(withArticle article: Article) {
        newsTitle.text = article.title
        newsDescription.text = article.description
        
        ImageManager.sharedInstance.getImage(strUrl: article.imageUrl) { image in
            self.newsImage.image = image
        }
        
    }
    
    override func prepareForReuse() {
        newsTitle.text = nil
        newsDescription.text = nil
        newsImage.image = nil;
    }
}
